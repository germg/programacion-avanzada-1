/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author ger
 */
public class MetrosKm extends Conversor {

    private static final Integer UN_KILOMETRO = 1000;

    /**
     * Convierte metros a kilometros
     *
     * @param metros Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor1Valor2(Double metros) {
        return metros / UN_KILOMETRO;
    }

    /**
     * Convierte kilometros a metros
     *
     * @param km Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor2Valor1(Double km) {
        return km * UN_KILOMETRO;
    }

    /**
     * Sobreescribe el método toString() asignando un mensaje.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Metros a KM";
    }

    /**
     * Método que se usará para asignar el valor del Label 1
     *
     * @return
     */
    @Override
    public String getLabelValor1() {
        return "Metros";
    }

    /**
     * Método que se usará para asignar el valor del Label 2
     *
     * @return
     */
    @Override
    public String getLabelValor2() {
        return "Kilómetros";
    }
}
