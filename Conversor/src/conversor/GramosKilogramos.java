/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author ger
 */
public class GramosKilogramos extends Conversor {

    public static final Integer UN_KILOGRAMO = 1000;

    /**
     * Convierte gramos a kilogramos
     *
     * @param gramos Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor1Valor2(Double gramos) {
        return gramos / UN_KILOGRAMO;
    }

    /**
     * Convierte kilogramos a gramos
     *
     * @param kilogramos Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor2Valor1(Double kilogramos) {
        return kilogramos * UN_KILOGRAMO;
    }

    /**
     * Sobreescribe el método toString() asignando un mensaje.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Gramos a Kilogramos";
    }

    /**
     * Método que se usará para asignar el valor del Label 1
     *
     * @return
     */
    @Override
    public String getLabelValor1() {
        return "Gramos";
    }

    /**
     * Método que se usará para asignar el valor del Label 2
     *
     * @return
     */
    @Override
    public String getLabelValor2() {
        return "Kilogramos";
    }
}
