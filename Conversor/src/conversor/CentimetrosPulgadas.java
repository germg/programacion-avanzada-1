/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author ger
 */
public class CentimetrosPulgadas extends Conversor {

    private static final double UNA_PULGADA = 2.54;

    /**
     * Convierte centimetos a pulgadas
     *
     * @param centimetros Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor1Valor2(Double centimetros) {
        return centimetros / UNA_PULGADA;
    }

    /**
     * Convierte pulgadas a centimetros
     *
     * @param pulgadas Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor2Valor1(Double pulgadas) {
        return pulgadas * UNA_PULGADA;
    }

    /**
     * Sobreescribe el método toString() asignando un mensaje.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Centímetros a Pulgadas";
    }

    /**
     * Método que se usará para asignar el valor del Label 1
     *
     * @return
     */
    @Override
    public String getLabelValor1() {
        return "Centímetros";
    }

    /**
     * Método que se usará para asignar el valor del Label 2
     *
     * @return
     */
    @Override
    public String getLabelValor2() {
        return "Pulgadas";
    }
}
