/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author ger
 */
public class BytesMegabytes extends Conversor {

    public static final Integer UN_MEGABYTE = 1024;

    /**
     * Convierte bytes a megabytes
     *
     * @param bytes Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor1Valor2(Double bytes) {
        return bytes / UN_MEGABYTE;
    }

    /**
     * Convierte megabytes a bytes
     *
     * @param megabytes Valor a convertir
     * @return
     */
    @Override
    public Double convertirValor2Valor1(Double megabytes) {
        return megabytes * UN_MEGABYTE;
    }

    /**
     * Sobreescribe el método toString() asignando un mensaje.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Bytes a Megabytes";
    }

    /**
     * Método que se usará para asignar el valor del Label 1
     *
     * @return
     */
    @Override
    public String getLabelValor1() {
        return "Bytes";
    }

    /**
     * Método que se usará para asignar el valor del Label 2
     *
     * @return
     */
    @Override
    public String getLabelValor2() {
        return "Megabytes";
    }
}
