/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testdao;

import dao.AlumnoDAOTXT;
import dao.DAO;
import dao.DAOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import persona.Alumno;
import persona.MiCalendario;
import persona.MiCalendarioException;
import persona.PersonaException;

/**
 *
 * @author ger
 */
public class TestDAO {

    private static final String FILENAME = "/home/ger/NetBeansProjects/programacion-avanzada-1/archivos/alumnos.txt";

    /**
     * @param args the command line arguments
     * @throws persona.MiCalendarioException
     * @throws persona.PersonaException
     */
    public static void main(String[] args) throws MiCalendarioException, PersonaException {
        testTXT();
    }

    private static void testTXT() {
        DAO alumnoDAOTXT;
        try {
            alumnoDAOTXT = new AlumnoDAOTXT(FILENAME);

            Alumno alumno = construirAlumno();
            
            alumnoDAOTXT.update(alumno);
            //alumnoDAOTXT.create(alumno);
            /*List<Alumno> listaAlumnos = alumnoDAOTXT.findAll();

            listaAlumnos.forEach((alu) -> {
                System.out.println(alu.toString());
            });*/

            //System.out.println(alumnoDAOTXT.read(34515225).toString());
            alumnoDAOTXT.close();
        } catch (DAOException | MiCalendarioException | PersonaException ex) {
            Logger.getLogger(TestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void testSQL() {

    }

    private static Alumno construirAlumno() throws MiCalendarioException, PersonaException {
        MiCalendario fechaNac = new MiCalendario(5, 1, 1965);
        MiCalendario fechaIng = new MiCalendario(12, 5, 2020);

        Alumno alumno = new Alumno(
                11111,
                "Lic.aaaaa",
                fechaNac,
                9,
                7.5,
                34522333,
                "abaaaaaaaa",
                "aaaaaaaa",
                fechaIng,
                'm');
        return alumno;
    }

}
