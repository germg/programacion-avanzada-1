/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author ger
 */
public class Alumno extends Persona {

    private int legajo;
    private String carrera;
    private MiCalendario fechaIng;
    private Integer cantMateriasAprobadas;
    private Double promedio;
    private char estado;

    /**
     * Constructor por defecto
     */
    public Alumno() {
    }

    /**
     * Constructor específico
     *
     * @param legajo int
     * @param carrera String
     * @param fechaIng MiCalendario
     * @param cantMateriasAprobadas Integer
     * @param promedio Double
     * @param dni Integer
     * @param nombre String
     * @param apellido String
     * @param fechaNac MiCalendario
     * @param sexo char
     * @param estado
     * @throws PersonaException
     * @throws persona.AlumnoException
     */
    public Alumno(int legajo, String carrera, MiCalendario fechaIng, Integer cantMateriasAprobadas, Double promedio, Integer dni, String nombre, String apellido, MiCalendario fechaNac, char sexo, char estado) throws PersonaException, AlumnoException {
        super(dni, nombre, apellido, fechaNac, sexo);
        this.setLegajo(legajo);
        this.setCarrera(carrera);
        this.setFechaIng(fechaIng);
        this.setCantMateriasAprobadas(cantMateriasAprobadas);
        this.setPromedio(promedio);
        this.setEstado(estado);
    }

    /**
     * Obtiene el legajo del alumno
     *
     * @return int
     */
    public int getLegajo() {
        return legajo;
    }

    /**
     * Obtiene la carrera del alumno
     *
     * @return String
     */
    public String getCarrera() {
        return carrera;
    }

    /**
     * Obtiene la fecha de ingreso del alumno
     *
     * @return MiCalendario
     */
    public MiCalendario getFechaIng() {
        return fechaIng;
    }

    /**
     * Obtiene la cantidad de materias aprobadas del alumno
     *
     * @return Integer
     */
    public Integer getCantMateriasAprobadas() {
        return cantMateriasAprobadas;
    }

    /**
     * Obtiene el promedio del alumno
     *
     * @return Double
     */
    public Double getPromedio() {
        return promedio;
    }

    /**
     * Asigna el legajo del alumno
     *
     * @param legajo int
     * @throws persona.AlumnoException
     */
    public final void setLegajo(int legajo) throws AlumnoException {
        if (legajo <= 0) {
            throw new AlumnoException("El legajo debe ser positivo.");
        }
        this.legajo = legajo;
    }

    /**
     * Asigna la carrera del alumno
     *
     * @param carrera String
     * @throws persona.AlumnoException
     */
    public final void setCarrera(String carrera) throws AlumnoException {
        if (carrera == null || carrera.trim().isEmpty()) {
            throw new AlumnoException("La carrera no puede ser nula ni vacía.");
        }
        this.carrera = carrera;
    }

    /**
     * Asigna la fecha de ingreso del alumno
     *
     * @param fechaIng MiCalendario
     */
    public final void setFechaIng(MiCalendario fechaIng) {
        this.fechaIng = fechaIng;
    }

    /**
     * Asigna la cantidad de materias aprobadas del alumno
     *
     * @param cantMateriasAprobadas Integer
     */
    public final void setCantMateriasAprobadas(Integer cantMateriasAprobadas) {
        this.cantMateriasAprobadas = cantMateriasAprobadas;
    }

    /**
     * Asigna el promedio del alumno
     *
     * @param promedio Double
     * @throws persona.AlumnoException
     */
    public final void setPromedio(Double promedio) throws AlumnoException {
        if (promedio <= 0) {
            throw new AlumnoException("El promedio debe ser positivo.");
        }

        this.promedio = promedio;
    }

    /**
     * Asigna el estado del alumno
     *
     * @param estado
     */
    public final void setEstado(char estado) {
        this.estado = estado;
    }

    /**
     * Obtiene el estado del alumno
     *
     * @return
     */
    public final char getEstado() {
        return this.estado;
    }

    /**
     * Convierte un array de campos a un Alumno
     *
     * @param camposAlumno String[]
     * @return Alumno
     * @throws PersonaException
     * @throws persona.MiCalendarioException
     */
    public static Alumno string2Alumno(String[] camposAlumno) throws PersonaException, MiCalendarioException {
        Alumno alumno = new Alumno();
        int index = 0;
        alumno.setDni(Integer.parseInt(camposAlumno[index++]));
        alumno.setNombre(camposAlumno[index++]);
        alumno.setApellido(camposAlumno[index++]);
        
        MiCalendario a = convertDate2MiCalendario(camposAlumno[index++]);
        alumno.setFechaNac(a);
        
        alumno.setSexo(camposAlumno[index++].charAt(0));
        alumno.setLegajo(Integer.parseInt(camposAlumno[index++]));
        alumno.setCarrera(camposAlumno[index++]);
        
        MiCalendario b = convertDate2MiCalendario(camposAlumno[index++]);
        alumno.setFechaIng(b);
        
        alumno.setCantMateriasAprobadas(Integer.parseInt(camposAlumno[index++]));
        alumno.setPromedio(Double.parseDouble(camposAlumno[index++].replace(",", ".")));
        alumno.setEstado(camposAlumno[index++].toUpperCase().charAt(0));

        return alumno;
    }

    /**
     * Convierte una fecha en string a MiCalendario
     *
     * @param fecha
     * @return
     * @throws MiCalendarioException
     */
    private static MiCalendario convertDate2MiCalendario(String fecha) throws MiCalendarioException {
        String[] arrayCampos = fecha.split("/");
        return new MiCalendario(Integer.parseInt(arrayCampos[0]), Integer.parseInt(arrayCampos[1]), Integer.parseInt(arrayCampos[2]));
    }

    /**
     * Sobreescribe el método toString para mostrar todos los datos del alumno +
     * los datos de la persona separados por un delimitador
     *
     * @return String
     */
    @Override
    public String toString() {

        String career = carrera.length() > 30 ? carrera.substring(0, 30) : carrera;

        return super.toString() + DELIMITADOR
                + String.format("%010d", legajo) + DELIMITADOR
                + String.format("%-30s", career) + DELIMITADOR
                + fechaIng + DELIMITADOR
                + String.format("%02d", cantMateriasAprobadas) + DELIMITADOR
                + String.format("%5.2f", promedio) + DELIMITADOR
                + estado;
    }
}
