/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author ger
 */
public abstract class Persona {

    public final static String DELIMITADOR = "\t";

    private Integer dni;

    private String nombre;

    private String apellido;

    private MiCalendario fechaNac;

    private char sexo;

    /**
     * Constructor por defecto. Llama al constructor padre.
     */
    public Persona() {
        super();
    }

    /**
     * Constrcutor específico solo con DNI
     *
     * @param dni Integer
     * @throws PersonaException
     */
    public Persona(Integer dni) throws PersonaException {
        setDni(dni);
    }

    /**
     * Constructor específico completo
     *
     * @param dni Integer
     * @param nombre String
     * @param apellido String
     * @param fechaNac MiCalendario
     * @param sexo char
     * @throws PersonaException
     */
    public Persona(Integer dni, String nombre, String apellido, MiCalendario fechaNac, char sexo) throws PersonaException {
        this.setDni(dni);
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setFechaNac(fechaNac);
        this.setSexo(sexo);
    }

    /**
     * Asigna el DNI. Valida que sea positivo y mayor que 0.
     *
     * @param dni Integer
     * @throws PersonaException
     */
    public final void setDni(Integer dni) throws PersonaException {

        if (dni <= 0 || dni > 99999999) {
            throw new PersonaException("El dni debe ser positivo.");
        }

        this.dni = dni;
    }

    /**
     * Asigna el nombre. Valida que no sea null y tampoco esté vacío.
     *
     * @param nombre String
     * @throws PersonaException
     */
    public final void setNombre(String nombre) throws PersonaException {

        if (nombre == null || nombre.trim().isEmpty()) {
            throw new PersonaException("El nombre no puede ser nulo ni vacío.");
        }

        this.nombre = nombre;
    }

    /**
     * Asigna el apellido. Valida que no sea null y tampoco esté vacío.
     *
     * @param apellido
     * @throws PersonaException
     */
    public final void setApellido(String apellido) throws PersonaException {

        if (apellido == null || apellido.trim().isEmpty()) {
            throw new PersonaException("El apellido no puede ser nulo ni vacío.");
        }

        this.apellido = apellido;
    }

    /**
     * Asigna la fecha de nacimiento
     *
     * @param fechaNac MiCalendario
     */
    public final void setFechaNac(MiCalendario fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * Asigna el sexo. valida que sea M o F.
     *
     * @param sexo char
     * @throws PersonaException
     */
    public final void setSexo(char sexo) throws PersonaException {

        sexo = Character.toLowerCase(sexo);

        if (sexo != 'f' && sexo != 'm') {
            throw new PersonaException("El sexo debe ser M o F.");
        }

        this.sexo = sexo;
    }

    /**
     * Obtiene el DNI de la persona
     *
     * @return Integer
     */
    public Integer getDni() {
        return dni;
    }

    /**
     * Obtiene el nombre de la persona
     *
     * @return String
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Obtiene el apellido de la persona
     *
     * @return String
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Obtiene la fecha de nacimiento de la persona
     *
     * @return MiCalendario
     */
    public MiCalendario getFechaNac() {
        return fechaNac;
    }

    /**
     * Obtiene el sexo de la persona
     *
     * @return
     */
    public char getSexo() {
        return sexo;
    }

    /**
     * Sobreescribe el método toString para mostrar la información de la persona
     * separada por un delimitador.
     *
     * @return String
     */
    @Override
    public String toString() {

        String name = nombre.length() > 20 ? nombre.substring(0, 20) : nombre;
        String lastName = apellido.length() > 20 ? apellido.substring(0, 20) : apellido;

        return String.format("%08d", dni) + DELIMITADOR
                + String.format("%-20s", name) + DELIMITADOR
                + String.format("%-20s", lastName) + DELIMITADOR
                + fechaNac + DELIMITADOR
                + sexo;
    }

}
