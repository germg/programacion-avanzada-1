/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author ger
 */
public class MiCalendarioException extends Exception {

    /**
     * Constructor específico que mostrará un mensaje
     * 
     * @param mensaje String
     */
    public MiCalendarioException(String mensaje) {
        super(mensaje);
    }
    
}
