/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author ger
 */
public class PersonaException extends Exception {

    /**
     * Constructor específico que mostrará un mensaje
     * 
     * @param mensaje String
     */
    public PersonaException(String mensaje) {
        super(mensaje);
    }

}
