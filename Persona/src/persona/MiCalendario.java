/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author ger
 */
public class MiCalendario extends GregorianCalendar {

    /**
     * Constructor por defecto
     */
    public MiCalendario() {
    }

    /**
     * Constructor para crear una fecha a partir de un long
     *
     * @param time
     */
    private MiCalendario(long time) {
        setTimeInMillis(time);
    }

    /**
     * Constructor específico
     *
     * @param dia int
     * @param mes int
     * @param anio int
     * @throws MiCalendarioException
     */
    public MiCalendario(int dia, int mes, int anio) throws MiCalendarioException {
        super(anio, mes - 1, dia);
        this.setLenient(false); // Le quita la parte permisiva

        try {
            get(MONTH);
        } catch (IllegalArgumentException e) {
            throw new MiCalendarioException("Error en la fecha: " + e.getMessage());
        }
    }

    /**
     * Obtiene el mes de la fecha actual
     *
     * @return int
     */
    public int getMes() {
        return this.get(Calendar.MONTH) + 1;
    }

    /**
     * Obtiene el día de la fecha actual
     *
     * @return int
     */
    public int getDia() {
        return this.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Obtiene el año de la fecha actual
     *
     * @return int
     */
    public int getAnio() {
        return this.get(Calendar.YEAR);
    }

    /**
     * Sobreescribe el método toString para mostrar la fecha de manera
     * personalizada dd/mm/aaaa
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("%02d/%02d/%04d", getDia(), getMes(), getAnio());
    }

    /**
     * Crea un MiCalendario a partir de un Date del package java.sql
     *
     * @param date
     * @return
     */
    public static MiCalendario convert2MiCalendario(Date date) {
        return new MiCalendario(date.getTime());
    }

    /**
     * Convierte una fecha de MiCalendario a una fecha del package java.sql
     *
     * @param fecha
     * @return
     */
    public static Date convert2SqlDate(MiCalendario fecha) {
        return new Date(fecha.getTimeInMillis());
    }
}
