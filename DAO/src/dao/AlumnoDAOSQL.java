/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persona.Alumno;
import persona.MiCalendario;
import persona.PersonaException;

/**
 *
 * @author ger
 */
public class AlumnoDAOSQL extends DAO<Alumno, Integer> {

    private Connection conn;
    private final PreparedStatement preparedStatementSelectOne;
    private final PreparedStatement preparedStatementInsert;
    private final PreparedStatement preparedStatementUpdate;
    private final PreparedStatement preparedStatementDelete;
    private final PreparedStatement preparedStatementSelectAll;
    private final PreparedStatement preparedStatementExists;

    /**
     * Constructor de la clase
     *
     * @param conexion
     * @param user
     * @param pwd
     * @throws DAOException
     */
    AlumnoDAOSQL(String conexion, String user, String pwd) throws DAOException {
        try {
            conn = DriverManager.getConnection(conexion, user, pwd);
        } catch (SQLException ex) {
            throw new DAOException("No se pudo conectar => " + ex.getMessage());
        }
        System.out.println("Conectado OK");

        String sqlSelectOne = "SELECT "
                + " `dni`,"
                + " `apellido`,"
                + " `nombre`,"
                + " `fecha_nac`,"
                + " `promedio`,"
                + " `cant_mat_aprob`,"
                + " `sexo`,"
                + " `legajo`,"
                + " `carrera`,"
                + " `fecha_ingreso`"
                + " FROM alumnos "
                + " WHERE dni = ?;";

        String sqlInsert = "INSERT INTO `alumnos`"
                + " (`dni`,"
                + " `apellido`,"
                + " `nombre`,"
                + " `fecha_nac`,"
                + " `promedio`,"
                + "`cant_mat_aprob`,"
                + " `sexo`,"
                + " `legajo`,"
                + " `carrera`,"
                + " `fecha_ingreso`)"
                + " VALUES"
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        String sqlUpdate = "UPDATE `alumnos`"
                + " SET"
                + " `dni` = ?,"
                + " `apellido` = ?,"
                + " `nombre` = ?,"
                + " `fecha_nac` = ?,"
                + " `promedio` = ?,"
                + " `cant_mat_aprob` = ?,"
                + " `sexo` = ?,"
                + " `legajo` = ?,"
                + " `carrera` = ?,"
                + " `fecha_ingreso` = ?"
                + " WHERE `dni` = ?;";

        String sqlDelete = "DELETE FROM `alumnos`"
                + " WHERE dni = ?;";

        String sqlSelectAll = "SELECT "
                + " `dni`,"
                + " `apellido`,"
                + " `nombre`,"
                + " `fecha_nac`,"
                + " `promedio`,"
                + " `cant_mat_aprob`,"
                + " `sexo`,"
                + " `legajo`,"
                + " `carrera`,"
                + " `fecha_ingreso`"
                + "FROM `alumnos`;";

        String sqlExists = "SELECT COUNT(*) cantidad FROM alumnos WHERE dni = ?;";

        try {
            preparedStatementSelectOne = conn.prepareStatement(sqlSelectOne);
            preparedStatementInsert = conn.prepareStatement(sqlInsert);
            preparedStatementUpdate = conn.prepareStatement(sqlUpdate);
            preparedStatementDelete = conn.prepareStatement(sqlDelete);
            preparedStatementSelectAll = conn.prepareStatement(sqlSelectAll);
            preparedStatementExists = conn.prepareStatement(sqlExists);

        } catch (SQLException ex) {
            throw new DAOException("No se pudo preparar la sentencia => " + ex.getMessage());
        }
    }

    /**
     * Inserta un alumno en la base de datos
     *
     * @param alumno
     * @throws DAOException
     */
    @Override
    public void create(Alumno alumno) throws DAOException {
        try {
            int index = 1;
            preparedStatementInsert.setInt(index++, alumno.getDni());
            preparedStatementInsert.setString(index++, alumno.getApellido());
            preparedStatementInsert.setString(index++, alumno.getNombre());
            preparedStatementInsert.setDate(index++, MiCalendario.convert2SqlDate(alumno.getFechaNac()));
            preparedStatementInsert.setDouble(index++, alumno.getPromedio());
            preparedStatementInsert.setInt(index++, alumno.getCantMateriasAprobadas());
            preparedStatementInsert.setString(index++, String.valueOf(alumno.getSexo()));
            preparedStatementInsert.setInt(index++, alumno.getLegajo());
            preparedStatementInsert.setString(index++, alumno.getCarrera());
            preparedStatementInsert.setDate(index, MiCalendario.convert2SqlDate(alumno.getFechaIng()));
            preparedStatementInsert.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("No se pudo insertar el alumno => " + ex.getMessage());
        }
    }

    /**
     * Obtiene un alumno por su DNI
     *
     * @param dni
     * @return
     * @throws DAOException
     */
    @Override
    public Alumno read(Integer dni) throws DAOException {
        try {
            Alumno alumno = null;

            this.preparedStatementSelectOne.setInt(1, dni);
            ResultSet resultSet = this.preparedStatementSelectOne.executeQuery();

            if (resultSet.next()) {
                alumno = new Alumno();
                alumno.setDni(resultSet.getInt("dni"));
                alumno.setApellido(resultSet.getString("apellido"));
                alumno.setNombre(resultSet.getString("nombre"));
                alumno.setFechaNac(MiCalendario.convert2MiCalendario(resultSet.getDate("fecha_nac")));
                alumno.setPromedio(resultSet.getDouble("promedio"));
                alumno.setCantMateriasAprobadas(resultSet.getInt("cant_mat_aprob"));
                alumno.setSexo(resultSet.getString("sexo").charAt(0));
                alumno.setCarrera(resultSet.getString("Carrera"));
                alumno.setLegajo(resultSet.getInt("legajo"));
                alumno.setFechaIng(MiCalendario.convert2MiCalendario(resultSet.getDate("fecha_ingreso")));
            }

            return alumno;
        } catch (SQLException ex) {
            throw new DAOException("No se pudo obtener el alumno de DNI: " + dni + " Error: " + ex.getMessage());
        } catch (PersonaException ex) {
            throw new DAOException("No se pudo crear al alumno: " + dni + " Error: " + ex.getMessage());
        }
    }

    /**
     * Actualiza un alumno
     *
     * @param alumno
     * @throws DAOException
     */
    @Override
    public void update(Alumno alumno) throws DAOException {
        try {
            int index = 1;
            preparedStatementUpdate.setInt(index++, alumno.getDni());
            preparedStatementUpdate.setString(index++, alumno.getApellido());
            preparedStatementUpdate.setString(index++, alumno.getNombre());
            preparedStatementUpdate.setDate(index++, MiCalendario.convert2SqlDate(alumno.getFechaNac()));
            preparedStatementUpdate.setDouble(index++, alumno.getPromedio());
            preparedStatementUpdate.setInt(index++, alumno.getCantMateriasAprobadas());
            preparedStatementUpdate.setString(index++, String.valueOf(alumno.getSexo()));
            preparedStatementUpdate.setInt(index++, alumno.getLegajo());
            preparedStatementUpdate.setString(index++, alumno.getCarrera());
            preparedStatementUpdate.setDate(index++, MiCalendario.convert2SqlDate(alumno.getFechaIng()));
            preparedStatementUpdate.setInt(index, alumno.getDni());
            preparedStatementUpdate.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("No se pudo actualizar el alumno => " + ex.getMessage());
        }
    }

    @Override
    public Boolean delete(Integer dni) throws DAOException {
        try {
            preparedStatementDelete.setInt(1, dni);
            preparedStatementDelete.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("No se pudo eliminar el alumno => " + ex.getMessage());
        }

        return true;
    }

    /**
     * Obtiene todos los alumnos existentes
     *
     * @return
     * @throws DAOException
     */
    @Override
    public List<Alumno> findAll() throws DAOException {
        List<Alumno> listaAlumnos = new ArrayList<>();

        try {
            Alumno alumno;

            ResultSet resultSet = this.preparedStatementSelectAll.executeQuery();

            while (resultSet.next()) {
                alumno = new Alumno();
                alumno.setDni(resultSet.getInt("dni"));
                alumno.setApellido(resultSet.getString("apellido"));
                alumno.setNombre(resultSet.getString("nombre"));
                alumno.setFechaNac(MiCalendario.convert2MiCalendario(resultSet.getDate("fecha_nac")));
                alumno.setPromedio(resultSet.getDouble("promedio"));
                alumno.setCantMateriasAprobadas(resultSet.getInt("cant_mat_aprob"));
                alumno.setSexo(resultSet.getString("sexo").charAt(0));
                alumno.setCarrera(resultSet.getString("Carrera"));
                alumno.setLegajo(resultSet.getInt("legajo"));
                alumno.setFechaIng(MiCalendario.convert2MiCalendario(resultSet.getDate("fecha_ingreso")));

                listaAlumnos.add(alumno);
            }
        } catch (SQLException ex) {
            throw new DAOException("No se pudieron obtener los alumnos => " + ex.getMessage());
        } catch (PersonaException ex) {
            throw new DAOException("No se pudo agregar al alumno => " + ex.getMessage());
        }

        return listaAlumnos;
    }

    /**
     * Verifica si existe un alumno por su DNI
     *
     * @param dni
     * @return
     * @throws dao.DAOException
     */
    @Override
    public boolean exists(Integer dni) throws DAOException {
        try {
            this.preparedStatementExists.setInt(1, dni);
            ResultSet resultSet = this.preparedStatementExists.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt("cantidad");
                return count != 0;
            }

            return false;

        } catch (SQLException ex) {
            throw new DAOException("No se pudo verificar la existencia del alumno con dni: " + dni + ". Error: " + ex.getMessage());
        }
    }

    /**
     * Cierra la conexión a la base de datos
     *
     * @throws DAOException
     */
    @Override
    public void close() throws DAOException {
        try {
            conn.close();
        } catch (SQLException ex) {
            throw new DAOException("No se pudo cerrar la conexión => " + ex.getMessage());
        }
    }
}
