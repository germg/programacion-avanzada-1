/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Map;

/**
 *
 * @author ger
 */
public class DAOAlumnoFactory {

    private static DAOAlumnoFactory instance;
    public static final String DAO_TXT = "DAO_TXT";
    public static final String DAO_SQL = "DAO_SQL";
    public static final String TIPO_DAO = "TIPO_DAO";
    public static final String FILE_NAME = "FILE_NAME";
    public static final String SQL_CONNECTION = "SQL_CONNECTION";
    public static final String SQL_CONNECTION_USER = "SQL_CONNECTION_USER";
    public static final String SQL_CONNECTION_PASSWORD = "SQL_CONNECTION_PASSWORD";

    /**
     * Constructor por defecto
     */
    private DAOAlumnoFactory() {
    }

    /**
     * Obtiene la instancia única de la fábrica
     * 
     * @return 
     */
    public static DAOAlumnoFactory getInstance() {
        if (instance == null) {
            // Solo informativo
            System.out.println("Creando instancia");
            instance = new DAOAlumnoFactory();
        } else {
            // Solo informativo
            System.out.println("La instancia ya existe");
        }

        return instance;
    }

    /**
     * Crea un DAO en base al tipo especificado
     * 
     * @param config
     * @return
     * @throws DAOException 
     */
    public DAO crearDAO(Map<String, String> config) throws DAOException {

        String tipoDao = config.get("TIPO_DAO");

        switch (tipoDao) {
            case DAO_TXT:
                return new AlumnoDAOTXT(config.get("FILE_NAME"));
            case DAO_SQL:
                return new AlumnoDAOSQL(config.get("SQL_CONNECTION"), config.get("SQL_CONNECTION_USER"), config.get("SQL_CONNECTION_PASSWORD"));
            default:
                throw new DAOException("Tipo de DAO no implementado.");

        }
    }
}
