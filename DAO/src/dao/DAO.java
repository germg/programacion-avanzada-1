/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;

/**
 *
 * @author ger
 */
public abstract class DAO<T, K> {

    /**
     * Crea una entidad
     *
     * @param entidad T
     * @throws dao.DAOException
     */
    public abstract void create(T entidad) throws DAOException;

    /**
     * Obtiene una entidad por su clave
     *
     * @param clave K
     * @return
     * @throws dao.DAOException
     */
    public abstract T read(K clave) throws DAOException;

    /**
     * Actualiza una entidad
     *
     * @param entidad T
     * @throws dao.DAOException
     */
    public abstract void update(T entidad) throws DAOException;

    /**
     * Elimina una entidad por su clave
     *
     * @param clave K
     * @return
     * @throws dao.DAOException
     */
    public abstract Boolean delete(K clave) throws DAOException;

    /**
     * Obtiene una lista de todas las entidades
     *
     * @return List<T>
     * @throws dao.DAOException
     */
    public abstract List<T> findAll() throws DAOException;

    /**
     * Verifica si existe una entidad por su clave
     *
     * @param clave K
     * @return boolean
     * @throws dao.DAOException
     */
    public abstract boolean exists(K clave) throws DAOException;
    
    /**
     * Cerrará una conexión a un archivo
     * 
     * @throws dao.DAOException
     */
    public abstract void close() throws DAOException;

}
