/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author ger
 */
public class DAOException extends Exception {

    /**
     * Constructor específico que mostrará un mensaje
     *
     * @param mensaje String
     */
    public DAOException(String mensaje) {
        super(mensaje);
    }

}
