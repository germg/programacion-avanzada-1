/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import persona.Alumno;
import persona.MiCalendarioException;
import persona.Persona;
import persona.PersonaException;

/**
 *
 * @author ger
 */
public class AlumnoDAOTXT extends DAO<Alumno, Integer> {

    private RandomAccessFile raf;

    /**
     * Constructor específico que se usará para crear el archivo que guardará
     * los datos
     *
     * @param filename String
     * @throws dao.DAOException
     */
    public AlumnoDAOTXT(String filename) throws DAOException {
        File file = new File(filename);

        try {
            raf = new RandomAccessFile(file, "rws");
        } catch (FileNotFoundException ex) {
            throw new DAOException("Archivo no encontrado ==> " + ex.getMessage());
        }
    }

    /**
     * Crea un alumno y lo guarda en un archivo
     *
     * @param alumno Alumno
     * @throws DAOException
     */
    @Override
    public void create(Alumno alumno) throws DAOException {
        try {
            if (this.exists(alumno.getDni())) {
                throw new DAOException("El alumno ingresado ya existe. DNI ==> " + alumno.getDni().toString());
            }

            // Se posiciona al final del archivo
            raf.seek(raf.length());

            // Guarda el alumno al final del archivo y agrega un enter
            String a = alumno.toString();
            raf.writeBytes(alumno.toString() + System.lineSeparator());

        } catch (IOException ex) {
            throw new DAOException("No se pudo crear el alumno => " + ex.getMessage());
        }
    }

    /**
     * Obtiene un alumno por su DNI
     *
     * @param dni
     * @return
     * @throws DAOException
     */
    @Override
    public Alumno read(Integer dni) throws DAOException {
        try {
            raf.seek(0);
            String linea;
            String[] campos;

            while ((linea = raf.readLine()) != null) {
                campos = linea.split(persona.Persona.DELIMITADOR);

                if (dni.equals(Integer.parseInt(campos[0]))) {
                    return Alumno.string2Alumno(campos);
                }
            }

        } catch (IOException | PersonaException | MiCalendarioException ex) {
            throw new DAOException("No se pudo obtener el alumno => " + ex.getMessage());
        }

        return null;
    }

    /**
     * Actualiza un alumno
     *
     * @param alumno
     * @throws DAOException
     */
    @Override
    public void update(Alumno alumno) throws DAOException {
        try {
            raf.seek(0);
            String linea;
            String campos[];
            Long puntero = 0L;
            while ((linea = raf.readLine()) != null) {
                campos = linea.split(Persona.DELIMITADOR);
                if (alumno.getDni().equals(Integer.valueOf(campos[0]))) {
                    raf.seek(puntero);
                    raf.writeBytes(alumno.toString());
                    return;
                }
                puntero = raf.getFilePointer();
            }
        } catch (IOException ex) {
            throw new DAOException("No se pudo actualizar el alumno: => " + ex.getMessage());
        }
    }

    /**
     * Elimina un alumno del archivo por su DNI
     *
     * @param dni
     * @return
     * @throws dao.DAOException
     */
    @Override
    public Boolean delete(Integer dni) throws DAOException {

        try {
            Alumno alumno = this.read(dni);
            alumno.setEstado('B');
            update(alumno);

            return true;
        } catch (DAOException ex) {
            throw new DAOException("No se pudo eliminar el alumno => " + ex.getMessage());
        }
    }

    /**
     * Obtiene todos los alumnos del archivo
     *
     * @return
     * @throws DAOException
     */
    @Override
    public List<Alumno> findAll() throws DAOException {
        try {
            raf.seek(0);
            String linea;
            String[] campos;

            List<Alumno> listaAlumnos = new ArrayList<>();

            while ((linea = raf.readLine()) != null) {
                campos = linea.split(persona.Persona.DELIMITADOR);
                Alumno alumno = Alumno.string2Alumno(campos);

                if (alumno.getEstado() == 'A') {
                    listaAlumnos.add(alumno);
                }
            }

            return listaAlumnos;

        } catch (IOException | PersonaException | MiCalendarioException ex) {
            throw new DAOException("No se pudieron obtener los alumnos => " + ex.getMessage());
        }
    }

    /**
     * Verifica si existe un alumno en el archivo por su DNI
     *
     * @param dni
     * @return
     * @throws DAOException
     */
    @Override
    public boolean exists(Integer dni) throws DAOException {
        try {
            //Se posiciona al principio para comenzar la busqueda
            raf.seek(0);

            String linea;
            String[] campos;

            while ((linea = raf.readLine()) != null) {
                campos = linea.split(persona.Persona.DELIMITADOR);

                if (dni.equals(Integer.parseInt(campos[0]))) {
                    return true;
                }
            }

        } catch (IOException ex) {
            throw new DAOException("No se pudo verificar la existencia del alumno => " + ex.getMessage());
        }

        return false;
    }

    /**
     * Cierra la conexion al archivo
     *
     * @throws DAOException
     */
    @Override
    public void close() throws DAOException {
        try {
            raf.close();
        } catch (IOException ex) {
            throw new DAOException("No se pudo cerrar el archivo ==> " + ex.getMessage());
        }
    }

}
