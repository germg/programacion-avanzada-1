/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testsingleton;

import dao.DAO;
import dao.DAOAlumnoFactory;
import dao.DAOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import persona.Alumno;
import persona.MiCalendario;
import persona.MiCalendarioException;
import persona.PersonaException;

/**
 *
 * @author ger
 */
public class TestSingleton {

    private static Random random;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        random = new Random();
        // Para testear DAOSql descomentar la siguiente linea
        testDAOSql(DAOAlumnoFactory.getInstance());

        // Para testear DAOTxt desconmetar la siguiente linea
        //testDAOTxt(DAOAlumnoFactory.getInstance());
    }

    /**
     * Método para realizar pruebas de DAOTxt
     *
     * @param dao
     */
    private static void testDAOTxt(DAOAlumnoFactory dao) {
        try {
            Map<String, String> config = createConfigDAO(false);
            DAO daoTxt = dao.crearDAO(config);

            // 1- Para el archivo de alumnos descomentar la siguiente linea
            //popularAlumnosTxt(daoTxt);

            // 2- Para leer un alumno descomentar la siguiente linea (colocar un DNI valido)
            //System.out.println("Alumno => " + daoTxt.read(34975659).toString());
            // 3- Para actualizar un alumno, asignar al siguiente alumno aleatorio, un DNI valido
            Alumno alumno = obtenerAlumnoRandom();
            alumno.setDni(34815285);
            //daoTxt.update(alumno);

            // 4- Para listar todos los alumnos descomentar las siguientes lineas
            //List<Alumno> listaAlumnos = daoTxt.findAll();
            /*listaAlumnos.forEach((a) -> {
                System.out.println("Alumno: " + a.toString());
            });*/

            // 5- Para verificar si existe un alumno, descomentar la siguiente lina (colocar un DNI valido)
            //System.out.println("Existe: " + (daoTxt.exists(34918039) ? "SI" : "NO"));

            // 6- Para eliminar si existe un alumno, descomentar la siguiente lina (colocar un DNI valido)
            //daoTxt.delete(34079454);
            daoTxt.close();

        } catch (DAOException | PersonaException ex) {
            Logger.getLogger(TestSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para realizar pruebas de DAOSql
     *
     * @param dao
     */
    private static void testDAOSql(DAOAlumnoFactory dao) {
        try {
            Map<String, String> config = createConfigDAO(true);
            DAO daoSql = dao.crearDAO(config);

            // 1- Para llenar la tabla de alumnos descomentar la siguiente linea
            popularAlumnosSql(daoSql);

            // 2- Para leer un alumno descomentar la siguiente linea (colocar un DNI valido)
            //System.out.println("Alumno => " + daoSql.read(34421043).toString());
            // 3- Para actualizar un alumno, asignar al siguiente alumno aleatorio, un DNI valido
            Alumno alumno = obtenerAlumnoRandom();
            alumno.setDni(34508084);
            //daoSql.update(alumno);

            // 4- Para listar todos los alumnos descomentar las siguientes lineas
            List<Alumno> listaAlumnos = daoSql.findAll();
            /*listaAlumnos.forEach((a) -> {
                System.out.println("Alumno: " + a.toString());
            });*/

            // 5- Para verificar si existe un alumno, descomentar la siguiente lina (colocar un DNI valido)
            System.out.println("Existe: " + (daoSql.exists(34145910) ? "SI" : "NO"));

            // 6- Para eliminar si existe un alumno, descomentar la siguiente lina (colocar un DNI valido)
            //daoSql.delete(34345429);

            daoSql.close();

        } catch (DAOException | PersonaException ex) {
            Logger.getLogger(TestSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Llena el archivo de alumnos para pruebas
     *
     * @param daoTxt
     */
    private static void popularAlumnosTxt(DAO daoTxt) {
        for (Integer i = 0; i < 10; i++) {

            try {
                daoTxt.create(obtenerAlumnoRandom());
            } catch (PersonaException | DAOException ex) {
                Logger.getLogger(TestSingleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Llena la tabla de alumnos para pruebas
     *
     * @param daoSql
     */
    private static void popularAlumnosSql(DAO daoSql) {
        for (Integer i = 0; i < 10; i++) {

            try {
                daoSql.create(obtenerAlumnoRandom());
            } catch (PersonaException | DAOException ex) {
                Logger.getLogger(TestSingleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Obtiene un alumno aleatorio para pruebas
     *
     * @return
     * @throws PersonaException
     */
    private static Alumno obtenerAlumnoRandom() throws PersonaException {
        return new Alumno(
                obtenerLegajoRandom(),
                obtenerCarreraRandom(),
                obtenerFechaRandom(true),
                obtenerCantidadMateriasRandom(),
                obtenerPromedioRandom(),
                obtenerDNIRandom(),
                obtenerNombreRandom(),
                obtenerApellidoRandom(),
                obtenerFechaRandom(false),
                obtenerSexoRandom(),
                obtenerEstadoRandom());
    }

    /**
     * Obtiene un estado aleatorio para pruebas
     * 
     * @return 
     */
    private static char obtenerEstadoRandom() {
        return random.nextInt(5) % 2 == 0 ? 'A' : 'B';
    }

    /**
     * Obtiene un promedio aleatorio para pruebas
     *
     * @return
     */
    private static Double obtenerPromedioRandom() {
        double start = 1;
        double end = 10;
        double randomDouble = new Random().nextDouble();
        double result = start + (randomDouble * (end - start));
        return result;
    }

    /**
     * Obtiene un DNI aleatorio para pruebas
     *
     * @return
     */
    private static Integer obtenerDNIRandom() {
        return random.nextInt(999999) + 34000000;

    }

    /**
     * Obtiene un sexo aleatorio para pruebas
     *
     * @return
     */
    private static char obtenerSexoRandom() {
        return random.nextInt(5) % 2 == 0 ? 'm' : 'f';

    }

    /**
     * Obtiene un nombre aleatorio para pruebas
     *
     * @return
     */
    private static String obtenerNombreRandom() {
        List<String> listaNombres = new ArrayList<>();
        listaNombres.add("Juan");
        listaNombres.add("Carlos");
        listaNombres.add("Jorge");
        listaNombres.add("Rigoberto");
        listaNombres.add("Pedro");

        return listaNombres.get(random.nextInt(5));
    }

    /**
     * Obtiene un apellido aleatorio para pruebas
     *
     * @return
     */
    private static String obtenerApellidoRandom() {
        List<String> listaApellidos = new ArrayList<>();
        listaApellidos.add("Perez");
        listaApellidos.add("Gomez");
        listaApellidos.add("Jerez");
        listaApellidos.add("Owen");
        listaApellidos.add("Wilson");

        return listaApellidos.get(random.nextInt(5));
    }

    /**
     * Obtiene una carrera aleatoria para pruebas
     *
     * @return
     */
    private static String obtenerCarreraRandom() {
        List<String> listaCarreras = new ArrayList<>();
        listaCarreras.add("Ingenieria en Informatica");
        listaCarreras.add("Abogacia");
        listaCarreras.add("Lic. en Gestion de Tecnologia");
        listaCarreras.add("Contador Publico");
        listaCarreras.add("Profesorado de Educacion Fisica");

        return listaCarreras.get(random.nextInt(5));
    }

    /**
     * Obtiene una cantidad de materias aleatoria para pruebas
     *
     * @return
     */
    private static Integer obtenerCantidadMateriasRandom() {
        return random.nextInt(42) + 2;
    }

    /**
     * Obtiene un legajo aleatorio para pruebas
     *
     * @return
     */
    private static Integer obtenerLegajoRandom() {
        return random.nextInt(99999) + 10000;
    }

    /**
     * Obtiene una fecha aleatoria para pruebas
     *
     * @return
     */
    private static MiCalendario obtenerFechaRandom(boolean esIngreso) {
        try {
            int anio = esIngreso ? random.nextInt(10) + 2000 : random.nextInt(10) + 1980;
            return new MiCalendario(random.nextInt(10) + 3, random.nextInt(11) + 1, anio);
        } catch (MiCalendarioException ex) {
            Logger.getLogger(TestSingleton.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Crea un mapa de configuracion para un DAOSql o un DAOTxt
     *
     * @param isSqlDAO
     * @return
     */
    private static Map<String, String> createConfigDAO(boolean isSqlDAO) {

        Map<String, String> config = new HashMap<>();

        if (isSqlDAO) {
            config.put(DAOAlumnoFactory.TIPO_DAO, DAOAlumnoFactory.DAO_SQL);
            config.put(DAOAlumnoFactory.SQL_CONNECTION, "jdbc:mysql://127.0.0.1:3306/universidad?useSSL=true");
            config.put(DAOAlumnoFactory.SQL_CONNECTION_USER, "desarrollo");
            config.put(DAOAlumnoFactory.SQL_CONNECTION_PASSWORD, "desarrollo");
        } else {
            config.put(DAOAlumnoFactory.TIPO_DAO, DAOAlumnoFactory.DAO_TXT);
            config.put(DAOAlumnoFactory.FILE_NAME, "/home/gemazza/java/programacion-avanzada-1/archivos/alumnos.txt");
        }

        return config;
    }

}
