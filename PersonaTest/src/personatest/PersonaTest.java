/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personatest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import persona.Alumno;
import persona.AlumnoException;
import persona.MiCalendario;
import persona.MiCalendarioException;
import persona.PersonaException;

/**
 *
 * @author ger
 */
public class PersonaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MiCalendario fechaNac;

        /* Persona persona1 = new Persona();

        

        try {
            fechaNac = new MiCalendario(10, 5, 1989);
            persona1.setDni(34515619);
            persona1.setApellido("Mazza");
            persona1.setNombre("German");
            persona1.setSexo('M');
            persona1.setFechaNac(fechaNac);
        } catch (PersonaException | MiCalendarioException ex) {
            System.out.println("TODO KO: " + ex.getMessage());
            
            Logger.getLogger(PersonaTest.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        System.out.println("TODO OK.");
        System.out.println("Persona1 con DNI = " + persona1.getDni());
        System.out.println("Persona1 con Nombre = " + persona1.getNombre());
        System.out.println("Persona1 con Apellido = " + persona1.getApellido());
        System.out.println("Persona1 con Sexo = " + persona1.getSexo());
        System.out.println("Persona1 con Fecha de nacimiento = " + persona1.getFechaNac());

        try {
            fechaNac = new MiCalendario(10, 5, 1989);
            Persona persona2 = new Persona(34001200);
            persona2.setApellido("Gentile");
            persona2.setNombre("German");
            persona2.setSexo('M');
            persona2.setFechaNac(fechaNac);

            System.out.println("TODO OK.");
            System.out.println("Persona2 con DNI = " + persona2.getDni());
            System.out.println("Persona2 con Nombre = " + persona2.getNombre());
            System.out.println("Persona2 con Apellido = " + persona2.getApellido());
            System.out.println("Persona2 con Sexo = " + persona2.getSexo());
            System.out.println("Persona2 con Fecha de nacimiento = " + persona2.getFechaNac());
        } catch (PersonaException | MiCalendarioException ex) {
            Logger.getLogger(PersonaTest.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        try {
            fechaNac = new MiCalendario(10, 5, 1989);
            Persona persona3 = new Persona(34222333, "German Maximiliano", "Mazza Gentile", fechaNac, 'M');

            System.out.println("TODO OK.");
            System.out.println("Persona3 con DNI = " + persona3.getDni());
            System.out.println("Persona3 con Nombre = " + persona3.getNombre());
            System.out.println("Persona3 con Apellido = " + persona3.getApellido());
            System.out.println("Persona3 con Sexo = " + persona3.getSexo());
            System.out.println("Persona3 con Fecha de nacimiento = " + persona3.getFechaNac());
        } catch (PersonaException | MiCalendarioException ex) {
            Logger.getLogger(PersonaTest.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
         */
        try {
            fechaNac = new MiCalendario(10, 5, 1989);

            List<Alumno> alumnos = new ArrayList<>();

            Alumno alumno1 = new Alumno(
                    123,
                    "Ingenieria",
                    fechaNac,
                    10,
                    7.5,
                    34515619,
                    "German",
                    "Mazza",
                    fechaNac,
                    'm');

            Alumno alumno2 = new Alumno(
                    321,
                    "Ingenieria",
                    fechaNac,
                    8,
                    5.5,
                    3222333,
                    "Juan",
                    "Perez",
                    fechaNac,
                    'm');

            Alumno alumno3 = new Alumno(
                    444,
                    "Ingenieria",
                    fechaNac,
                    5,
                    8.0,
                    3555777,
                    "",
                    "Cazzo",
                    fechaNac,
                    'f');

            alumnos.add(alumno1);
            alumnos.add(alumno2);
            alumnos.add(alumno3);
            
            

            alumnos.forEach((alumno) -> {
                System.out.println("Alumno: " + alumno);
            });
            /*
            System.out.println("TODO OK.");
            System.out.println("Persona3 con DNI = " + alumno.getDni());
            System.out.println("Persona3 con Nombre = " + alumno.getNombre());
            System.out.println("Persona3 con Apellido = " + alumno.getApellido());
            System.out.println("Persona3 con Sexo = " + alumno.getSexo());
            System.out.println("Persona3 con Fecha de nacimiento = " + alumno.getFechaNac());
             */
            
            for(Alumno f : alumnos){
            }
            
        } catch (PersonaException | MiCalendarioException ex) {
            if (ex instanceof AlumnoException) {
                System.err.println("AlumnoException");
            } else if (ex instanceof PersonaException) {
                System.err.println("PersonaException");
            } else {
                Logger.getLogger(PersonaTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
